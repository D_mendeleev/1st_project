$(document).ready(function(){
  $('.your-class').slick({
    slidesPerRow: 2,
    slidesToShow: 2,
    infinite: true
  });
});

$('.autoplay').slick({
  slidesToShow: 3,
  slidesToScroll: 3,
  autoplay: true,
});